NAME = ft_ssl

RED 	= \033[0;31m
GREEN 	= \033[1;32m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m

SRC = 	src/main.c \
		src/helpers/helpers.c \
		src/hashing/md5/md5.c \
		src/hashing/sha256/sha256.c \
		src/hashing/helpers.c \
		src/hashing/hashing.c \
		src/hashing/hashing_parse_argument.c \
		src/cipher/base64/base64.c \
		src/cipher/base64/base64_helpers.c \
		src/cipher/des/des.c \
		src/cipher/des/algorithm.c \
		src/cipher/des/des_helpers.c \
		src/cipher/cipher.c \
		src/cipher/parse_arguments.c \


OBJ = $(SRC:.c=.o)

LIBFT = src/libft/libft.a

HEADERS = -I./src/libft/ -I./src/

COMPILER = gcc

FLAGS = -g -Wall -Wextra -Werror

all: $(OBJ)
	@ make -C src/libft/ --no-print-directory
	@ $(COMPILER) $(FLAGS) $(OBJ) $(LIBFT) -o $(NAME)
	@ echo "${GREEN}Compile binary file was successful${DEF}"

%.o : %.c
	@ $(COMPILER) $(FLAGS) $(HEADERS) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -f $(OBJ)
	@ make clean -C src/libft/ --no-print-directory
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -f $(NAME)
	@ make fclean -C src/libft/ --no-print-directory
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all