#include "base64_hidden.h"
#include "base64_header.h"

static void    encode_part(char *dst, char *str)
{
    dst[0] = B64[((unsigned char)str[0] >> 2)];
    dst[1] = B64[(((unsigned char)str[0] & 3) << 4) | ((unsigned char)str[1] >> 4)];
    dst[2] = B64[(((unsigned char)str[1] & 15) << 2) | ((unsigned char)str[2] >> 6)];
    dst[3] = B64[((unsigned char)str[2] & 63)];
}

static void    decode_part(char *dst, char *str)
{
    dst[0] = (find_index(B64, str[0]) << 2) | (find_index(B64, str[1]) >> 4);
    dst[1] = (find_index(B64, str[1]) << 4) | (find_index(B64, str[2]) >> 2);
    dst[2] = (find_index(B64, str[2]) << 6) | find_index(B64, str[3]);
}

char*   base64_encode(char *str, unsigned long int *len)
{
    char    *tmp;
    char    *res;
    size_t  i;
    int     pad;
    int     n;

    res = "";
    if (str == NULL || *len == 0)
        return res;
    pad = 0;
    while ((*len + pad) % 3 != 0)
        pad++;
    tmp = ft_strnew(*len + pad);
    ft_memcpy(tmp, str, *len);
    res = ft_strnew(((*len + pad) / 3) * 4 + (((*len + pad) / 3) * 4) / 64);
    n = 0;
    i = 0;
    while (i < ((*len + pad) / 3))
    {
        if (i != 0 && ((i * 4) % 64) == 0)
            res[i * 4 + n++] = '\n';
        encode_part(res + ((i * 4) + n), tmp + (i * 3));
        i++;
    }
    *len = ((*len + pad) / 3) * 4;
    i = (i * 4) + n;
    while (pad--)
        res[--i] = '=';
    free(tmp);
    return res;
}

char*  base64_decode(char *str, unsigned long int *len)
{
    char    *res;
    char    *tmp;
    size_t  i;

    res = "";
    if (str == NULL || *len == 0)
        return res;
    tmp = str;
    str = delete_new_lines(str, len);
    (tmp != str) ? free(tmp) : 0;
    if (*len % 4 != 0 || validation(str) == 0)
        return res;
    tmp = ft_strdup(str);
    res = ft_strnew((*len / 4) * 3);
    tmp[*len - 1] = (tmp[*len - 1] == '=') ? B64[0] : tmp[*len - 1];
    tmp[*len - 2] = (tmp[*len - 2] == '=') ? B64[0] : tmp[*len - 2];
    i = 0;
    while (++i < (*len / 4))
    {
        decode_part(res + (i * 3), tmp + (i * 4));
        i++;
    }
    free(tmp);
    *len = ((*len / 4) * 3);
    return res;
}
