#ifndef BASE64_H
#define BASE64_H

#define BASE64  "base64"

char*   base64_encode(char *str, unsigned long int *len);
char*   base64_decode(char *str, unsigned long int *len);

#endif