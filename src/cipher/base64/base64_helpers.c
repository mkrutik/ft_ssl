#include "base64_hidden.h"
#include "base64_header.h"


int     find_index(char *src, char c)
{
    int i;

    i = -1;
    while (src[++i] != '\0')
        if (src[i] == c)
            return i;
    return -1;
}

int   validation(char *src)
{
    int i;

    i = -1;
    while (src[++i] != '\0')
        if (src[i] != '=' && find_index(B64, src[i]) == -1)
            return 0;
    return 1;
}

char*    delete_new_lines(char *str, size_t *len)
{
    int     i;
    int     j;

    i = 0;
    j = 0;
    while (str[i] != '\0')
        (str[i++] == '\n') ? j++ : 0;
    if (j == 0)
        return str;
    *len -= j;
    i = -1;
    j = 0;
    while (str[++i] != '\0')
        (str[i] != '\n') ? (str[j++] = str[i]) : 0;
    str[j] = '\0';
    return str;
}