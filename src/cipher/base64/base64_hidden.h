#ifndef BASE64_HIDDEN_H
#define BASE64_HIDDEN_H

#include "../../libft/libft.h"

#define B64 "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

int     find_index(char *src, char c);
int     validation(char *src);
char*   delete_new_lines(char *str, size_t *len);

#endif