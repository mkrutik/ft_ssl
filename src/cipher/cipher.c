#include "cipher.h"

static void s_init(t_ciph *d)
{
    d->cmd = NULL;
    d->in_fname = NULL;
    d->inp = NULL;
    d->i_len = 0;
    d->out_fd = 0;
    d->out_fname = NULL;
    d->key = NULL;
    d->vec = NULL;
    d->res = NULL;
}

void    cleanup(t_ciph *d)
{
    (d->i_len != 0) ? free(d->inp) : 0;
    (d->res != NULL || !ft_strcmp(d->res, "")) ? free(d->res) : 0;
    (d->vec && d->flags.v != 1) ? free(d->vec) : 0;
    (d->key && d->flags.k != 1) ? free(d->key) : 0;
    free(d);
}

void    cipher(char **argv, int argc)
{
    t_ciph *d;
    t_des   tmp;

    d = (t_ciph*)malloc(sizeof(t_ciph));
    s_init(d);
    cipher_args(argc, argv, d);
    if (!ft_strcmp(d->cmd, BASE64) && d->flags.e)
        d->res  = base64_encode(d->inp, &d->i_len);
    else if (!ft_strcmp(d->cmd, BASE64) && d->flags.d)
        d->res  = base64_decode(d->inp, &d->i_len);
    else
    {
        tmp.command = d->cmd;
        tmp.mod = (d->flags.e) ? 1 : -1;
        tmp.base64 = (d->flags.a) ? 1 : 0;
        tmp.inp = d->inp;
        tmp.pad = (d->flags.p) ? 1 : 0;
        tmp.d_len = d->i_len;
        tmp.key = d->key;
        tmp.iv = d->vec;
        d->res =  des(&tmp);  
    }
    (write_to_file(d->out_fname, d->res, d->i_len) == -1) ? error("can`t write to file") : 0;
    cleanup(d);
}
