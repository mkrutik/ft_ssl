#ifndef CIPHER_H
#define CIPHER_H

#include "../libft/libft.h"
#include "../helpers/helpers_header.h"
#include "base64/base64_header.h"
#include "des/des_header.h"

#define ENCRYPT  "-e"
#define DECRYPT  "-d"
#define FILE_IN  "-i"
#define FILE_OUT "-o"

#define USE_BASE64  "-a"
#define KEY         "-k"

#define VEC "-v"

#define NO_PAD "-nopad"

typedef struct  s_flags
{
    unsigned e: 1;
    unsigned d: 1;
    unsigned i: 1;
    unsigned o: 1;
    unsigned a: 1;
    unsigned k: 1;
    unsigned v: 1;
    unsigned p: 1;
}               t_flags;

typedef	struct  s_ciph
{
    t_flags     flags;
    char        *cmd;
    char        *in_fname;
    char        *inp;
    size_t      i_len;
    char        *out_fname;
    int         out_fd;
    char        *key;
    char        *vec;
    char        *res;
}               t_ciph;

void    cipher(char **argv, int argc);
void    cipher_args(int n_arg, char **arg, t_ciph *d);

#endif