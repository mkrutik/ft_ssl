#include "des_hidden.h"
#include "des_tables.h"

uint64_t    permutation(uint64_t input, const unsigned int *tab, int len)
{
    uint64_t    res;
    int         i;

    res = 0;
    i = -1;
    while (i++ < (len - 1))
        res |= ((input >> (64 - tab[i])) << 63) >> i;
    return res;
}

uint64_t*   key_generation(char *key)
{
    uint64_t    *res;
    uint64_t    left;
    uint64_t    right;
    uint64_t    n;
    int         i;

    (ft_strlen(key) > 16) ? error("hex string is too long\ninvalid hex key value") : 0;
    (hex_to_ull(key, &left) != 1) ? error("non-hex digit\ninvalid hex key value") : 0;
    res = (uint64_t*)malloc(sizeof(uint64_t) * 16);
    n = permutation(left, pc_1, 56);
    left =  n >> 36;
    right = (n << 28) >> 36;
    i = -1;
    while (++i < 16)
    {
        n = left_shift[i];
        left = ((left << 36) >> (64 - n)) | (((left << n) << 36) >> 36);
        right = ((right << 36) >> (64 - n)) | (((right << n) << 36) >> 36);
        res[i] = permutation(((left << 36) | (right << 8)), pc_2, 48);
    }
    return res;
}

uint64_t  magic_func(uint64_t n, uint64_t k)
{
    uint64_t        res;
    uint64_t        row;
    uint64_t        col;
    unsigned int    i;

    n = permutation(n, e_bits, 48) ^ k;
    res = 0;
    i = 0;
    while (i < 48)
    {
        row = (n << i) >> 58;
        col = (row >> 1) & 15;
        row = (row & 1) | ((row & 32) >> 4);
        res = (res << 4) | s_boxes[i / 6][row][col];
        i += 6;
    }
    return permutation((res << 32), pt, 32);
}

uint64_t   feistel(uint64_t data, uint64_t *k, int mod)
{
    uint64_t    L;
    uint64_t    R;
    uint64_t    tmp;
    int         i;

    L = (data >> 32) << 32;
    R = data << 32;
    i = -1;
    while (++i < 16)
    {
        tmp = R;
        R = L ^ ((mod == 1) ? magic_func(R, k[i]) : magic_func(R, k[15 - i]));
        L = tmp;
    }
    return ((R) | (L >> 32));
}

uint64_t    block(uint64_t data, uint64_t *k, uint64_t *iv, int n_v, int mod)
{
    uint64_t res;

    res = data ^ ((n_v && mod == 1) ? *iv : 0);
    res = permutation(res, ip_1, 64);
    res = feistel(res, k, mod);
    res = permutation(res, ip_2, 64) ^ ((n_v && mod == -1) ? *iv : 0);
    if (n_v)
        *iv = (mod == 1) ? res : data;
    return res;
}