
#include "des_hidden.h"

char*   des_doer(t_des *src, int need_v)
{
    char            *res;
    uint64_t        *keys;
    long long int   len;
    uint64_t        vec;
    size_t          i;

    keys = key_generation(src->key);
    (src->iv && hex_to_ull(src->iv, &vec) != 1) ? error("non-hex digit\ninvalid hex iv value") : 0;
    src->d_len = (src->mod == -1) ? (src->d_len - (src->d_len % 8)) : src->d_len;
    res = ft_strnew(src->d_len);
    i = 0;
    len = src->d_len;
    while (i < src->d_len)
    {
        ull_to_array(res + i, block(array_to_ull(src->inp + i, len), keys, &vec, need_v, src->mod));
        i += 8;
        len -= 8;
    }
    free(keys);
    return res;
}

char*   des(t_des *d)
{
    char    *res;
    char    *p;

    if (d->inp == NULL || (d->pad == 1 && d->inp[0] == '\0'))
        return "";
    res = "des: unknown command";
    (d->iv && ft_strlen(d->iv) > 32) ? error("hex string is too long\ninvalid hex iv value"): 0;
    p = NULL;
    if (d->mod == -1 && d->base64 == 1)
        d->inp = base64_decode(d->inp, &d->d_len);
    if (d->mod == 1 && d->pad == 0)
        d->inp = add_padding(d->inp, &d->d_len, 8);
    p = d->inp;
    if (d->mod == 1 && d->d_len % 8 != 0)
        error("bad decrypt\ntext must be multiple of block length");
    (!ft_strcmp(d->command, DES_CBC)) ? (res = des_doer(d, 1)) : 0;
    (!ft_strcmp(d->command, DES_ECB)) ? (res = des_doer(d, 0)) : 0;
    if ((d->mod == -1 && d->pad == 0) || (d->mod == 1 && d->base64 == 1))
    {
        p = res;
        res = (d->mod == 1 && d->base64 == 1) ? base64_encode(res, &d->d_len) : del_padding(res, 8, &d->d_len);
        free(p);
    }
    return res;
}