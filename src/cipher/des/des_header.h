#ifndef DES_HEADER_H
#define DES_HEADER_H

#include "../base64/base64_header.h"

#define DES_ECB     "des-ecb"
#define DES_CBC     "des-cbc"
#define DES         "des"

typedef struct          s_des{
    char                *command;
    int                 mod;    // 1) encrypt or -1) decrypt
    int                 base64; // need base64 1 - true another false
    int                 pad;    // padding 1 - true, another false
    char                *inp;
    unsigned long int   d_len;
    char                *key;
    char                *iv;
}                       t_des;

char*   des(t_des *src);

#endif