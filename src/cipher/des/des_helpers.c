/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_helpers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrut <mkrut@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 12:57:20 by mkrut             #+#    #+#             */
/*   Updated: 2018/04/03 13:32:52 by mkrut            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "des_hidden.h"

uint64_t  array_to_ull(char *str, size_t len)
{
    uint64_t    res;
    int         i;
    int         n;

    if (str == NULL)
        return 0;
    n = (len > 8) ? 8 : len;
    res = 0;
    i = 0;
    while (i < n)
        res = (res << 8) | (unsigned char)(str[i++]);
    return res;
}

void    ull_to_array(char *dst, uint64_t x)
{
    dst[0] = x >> 56;
    dst[1] = (x << 8) >> 56;
    dst[2] = (x << 16) >> 56;
    dst[3] = (x << 24) >> 56;
    dst[4] = (x << 32) >> 56;
    dst[5] = (x << 40) >> 56;
    dst[6] = (x << 48) >> 56;
    dst[7] = (x << 56) >> 56;
}

int hex_to_ull(char *s, uint64_t *res)
{
    int i;

    *res = 0;
    if (s == NULL || s[0] == '\0')
        return 1;
    i = -1;
    while (s[++i] != '\0')
        if (!(s[i] >= '0' && s[i] <= '9') && !(s[i] >= 'a' &&
         s[i] <= 'f') && !(s[i] >= 'A' && s[i] <= 'F'))
            return -1;
    i = 0;
    while (s[i] != '\0' && i < 16)
    {
        if (s[i] >= '0' && s[i] <= '9')
            *res = (*res * 16) + (s[i] - '0');
        else if (s[i] >= 'a' && s[i] <= 'f')
            *res = (*res * 16) + (s[i] - 'a') + 10;
        else if (s[i] >= 'A' && s[i] <= 'F')
            *res = (*res * 16) + (s[i] - 'A') + 10;
        i++;
    }
    while (i++ < 16)
        *res = *res << 4;
    return 1;
}

char*   add_padding(char *data, unsigned long int *len, unsigned int b_size)
{
    char    *res;
    int     n;

    n = (*len % b_size == 0) ? (b_size) : (b_size - (*len % b_size));
    res = ft_strnew(*len + n);
    ft_memcpy(res, data, *len);
    ft_memset(res + *len, n, n);
    *len += n;
    return res;
}

char*   del_padding(char *data, int block_size, unsigned long int *len)
{
    char    *res;
    int     new_len;
    int     n;
    int     i;

    if (*len == 0)
        return ft_strdup(data);
    new_len = *len;
    n = data[--new_len];
    if (n < 0 || n > block_size)
        return ft_strdup(data);
    i = n;
    while (new_len >= 0 && i-- != 0)
        if (data[new_len--] != n)
            return data;
    new_len++;
    res = ft_strnew(new_len);
    ft_memcpy(res, data, new_len);
    *len = new_len;
    return res;
}
