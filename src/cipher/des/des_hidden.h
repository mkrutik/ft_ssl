/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hiden.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrut <mkrut@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 11:21:15 by mkrut             #+#    #+#             */
/*   Updated: 2018/04/03 13:22:54 by mkrut            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DES_HIDDEN_H
#define DES_HIDDEN_H

#include <stdint.h>

#include "des_header.h"
#include "../../helpers/helpers_header.h"

uint64_t    block(uint64_t d, uint64_t *k, uint64_t *iv, int n_v, int mod);
uint64_t*   key_generation(char *key);
char*       add_padding(char *data, unsigned long int *len, unsigned int b_size);
char*       del_padding(char *data, int block_size, unsigned long int *len);
uint64_t    array_to_ull(char *str, size_t len);
void        ull_to_array(char *dst, uint64_t x);
int         hex_to_ull(char *str, uint64_t *res);

#endif