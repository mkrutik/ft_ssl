#include "cipher.h"

static int flag(char *str)
{
    if (!ft_strcmp(str, ENCRYPT) || !ft_strcmp(str, DECRYPT) || 
        !ft_strcmp(str, FILE_IN) || !ft_strcmp(str, FILE_OUT) || 
        !ft_strcmp(str, USE_BASE64) || !ft_strcmp(str, KEY) ||
        !ft_strcmp(str, VEC) || !ft_strcmp(str, NO_PAD))
        return 1;
    return 0;
}

static void   parse_flags(int argc, char **a, int i, t_ciph *d)
{
    while (++i < argc)
    {
        (flag(a[i]) != 1) ? error(ft_strjoin(a[i], " is unknown option")) : 0;
        if (!ft_strcmp(a[i], ENCRYPT))
            d->flags.e = 1;
        else if (!ft_strcmp(a[i], DECRYPT))
            d->flags.d = 1;
        else if (!ft_strcmp(a[i], USE_BASE64))
            d->flags.a = 1;
        else if (!ft_strcmp(a[i], NO_PAD))
            d->flags.p = 1;
        else if (!ft_strcmp(a[i], FILE_IN) && (d->flags.i = 1))
            (++i >= argc || flag(a[i])) ? error("-i missing file name") : (d->in_fname = a[i]);
        else if (!ft_strcmp(a[i], FILE_OUT) && (d->flags.o = 1))
            (++i >= argc || flag(a[i])) ? error("-o missing file name") : (d->out_fname = a[i]);
        else if (!ft_strcmp(a[i], KEY) && (d->flags.k = 1))
            (++i >= argc || flag(a[i])) ? error("-k missing argument") : (d->key = a[i]);
        else if (!ft_strcmp(a[i], VEC) && (d->flags.v = 1))
            (++i >= argc || flag(a[i])) ? error("-v missing argument") : (d->vec = a[i]);
    }
    d->flags.e = (d->flags.d == 0) ? 1 : d->flags.e;
}

static void check_args(t_ciph *d)
{
    (d->flags.e && d->flags.d) ? error("not allowed use both -d and -e flags") : 0;
    if (!ft_strcmp(d->cmd, BASE64))
        (d->flags.a || d->flags.k || d->flags.v || d->flags.p) ? error("use not allowed flags for base64") : 0;
    else if (!ft_strcmp(d->cmd, DES_ECB))
        (d->flags.k == 0) ? d->key = getpass("enter des key in hex: ") : 0;
    else
    {
        (d->flags.k == 0) ? d->key = getpass("enter des key in hex: ") : 0;        
        (d->flags.v == 0) ? d->vec = getpass("enter initial vector: ") : 0;
    }
    d->inp = read_in(((d->flags.i) ? d->in_fname : NULL), &d->i_len);
    (d->inp == NULL) ? error("can`t read from file") : 0;
}

void cipher_args(int n_arg, char **arg, t_ciph *d)
{
    if (!ft_strcmp(arg[1], BASE64) || !ft_strcmp(arg[1], DES_ECB) ||
        !ft_strcmp(arg[1], DES_CBC))
        d->cmd = arg[1];
    else if (!ft_strcmp(arg[1], DES))
        d->cmd = DES_CBC; 
    parse_flags(n_arg,arg, 1, d);
    check_args(d);
}