#include "hashing_helpers.h"
#include "hashing.h"

t_op*    new_op(t_helper *h, t_op *prev, int inc)
{
    t_op    *res;
    
    res = (t_op*)malloc(sizeof(t_op));
    res->n = NULL;
    res->inp = NULL;
    res->out = NULL;
    res->err = NULL;
    res->next = NULL;
    res->f.p = 0;
    res->f.q = 0;
    res->f.s = 0;
    res->f.r = 0;
    (inc == 1) ? (h->op_len++) : (h->op_len = 0);
    (prev != NULL) ? (prev->next = res) : 0;
    return res;
}

void    clear_op(t_helper *d)
{
    t_op    *del;

    while (d->op != NULL)
    {
        if (d->op->err == NULL)
            free(d->op->out);
        if (d->op->err && d->op->len != 0)
            free(d->op->inp);
        else
            free(d->op->err);
        del = d->op;
        d->op = d->op->next;
        free(del);
    }
    free(d);
}

void    hashing(char *command, char **arg, int n_arg)
{
    t_helper    *d;
    t_op        *p;
    char*       (*function)(char*, size_t);

    d = (t_helper*)malloc(sizeof(t_helper));
    (!ft_strcmp(command, MD5)) ? ( function = &md5) : 0;
    (!ft_strcmp(command, SHA256)) ? ( function = &sha256) : 0;
    d->f_arg = 0;
    d->op = new_op(d, NULL, 0);
    parse_args(arg, n_arg, d);
    p = d->op;
    while (d->op_len > 0 && p != NULL)
    {
        (p->err == NULL) ? (p->out = (function)(p->inp, p->len)) : 0;
        p = p->next;
    }
    command = ft_strdup(command);
    print_res(command, d->op, d->op_len);
    free(command);
    clear_op(d);
}
