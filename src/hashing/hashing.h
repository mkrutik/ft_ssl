#ifndef HASHING_H
#define HASHING_H

#include "md5/md5_header.h"
#include "sha256/sha256_header.h"

void    hashing(char *command, char **arg, int n_arg);

#endif