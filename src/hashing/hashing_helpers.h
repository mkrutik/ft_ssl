#ifndef HASING_HELPERS_H
#define HASING_HELPERS_H

#include <stdint.h>
#include "../libft/libft.h"
#include "../helpers/helpers_header.h"

typedef struct  s_flag
{
    unsigned    p: 1;
    unsigned    s: 1;
    unsigned    q: 1;
    unsigned    r: 1;
}               t_flag;

typedef	struct  s_op 
{
    char        *n;
    char        *inp;
    size_t      len;
    char        *out;
    char        *err;
    t_flag      f;
    struct s_op *next;
}               t_op;

typedef	struct  s_helper
{
    int         f_arg;         
    int         op_len;
    t_op        *op;
}               t_helper;

uint8_t*    padding(char *data, size_t *len, int endian);
char*       blocks_to_hex(uint32_t *blocks, int len, int endian);
void        print_res(char *c, t_op *op, int len);
t_op*       new_op(t_helper *h, t_op *prev, int inc);
void    parse_args(char **arg, int n_arg, t_helper *d);

#endif