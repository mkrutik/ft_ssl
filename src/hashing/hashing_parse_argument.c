#include "hashing_helpers.h"

static void    parse_s(t_op *p, char **arg, int argc, int i)
{
    p->f.s = 1;
    if (i + 1 == argc)
        p->err = ft_strdup("flag -s required next string argument for hashing");
    else
    {
        p->inp = ft_strdup(arg[++i]);
        p->len = ft_strlen(p->inp);
        p->n = p->inp;
    }
}

static void    parse_file(t_helper *d, t_op *p, char **arg, int i)
{
    d->f_arg = 1;
    p->n = arg[i];
    p->inp = read_in(p->n, &p->len);
    if (p->inp == NULL)
        p->err = ft_strjoin(p->n, ": No such file or directory");
}

static void    parse_inp(t_helper *d, t_op *p, int mod)
{
    p->inp = read_in(NULL, &p->len);
    (mod == 1) ? (p->f.p = 1) : ( d->op_len = 1);   
}

void    parse_args(char **arg, int n_arg, t_helper *d)
{
    t_op    *p;
    int     i;
    
    i = 1;
    p = d->op;
    while (++i < n_arg)
    {
        if (d->f_arg == 0 && !ft_strcmp(arg[i], "-p"))
            parse_inp(d, p, 1);
        else if (d->f_arg == 0 && !ft_strcmp(arg[i], "-q"))
            p->f.q = 1;
        else if (d->f_arg == 0 && !ft_strcmp(arg[i], "-r"))
            p->f.r = 1;
        else if (d->f_arg == 0 && !ft_strcmp(arg[i], "-s"))
            parse_s(p, arg, n_arg, i++);
        else
            parse_file(d, p, arg, i);   
        if (ft_strcmp(arg[i], "-q") && ft_strcmp(arg[i], "-r"))
            p = new_op(d, p, 1);

    }
    (d->op_len == 0) ? parse_inp(d, p, 2) : 0;
}