#include "hashing_helpers.h"

uint8_t*   padding(char *data, size_t *len, int endian)
{
    uint8_t     *res;
    size_t      new_len;
    int         i;

    new_len = *len + 1;
    while ((new_len + 8) % 64 != 0)
        new_len++;
    res = (uint8_t*)ft_strnew(new_len + 8);
    ft_memcpy(res, data, *len);
    res[*len] = 0x80;
    *len *= 8;
    i = -1;
    while (++i < 8)
        res[new_len + ((endian != 1) ? (7 - i) : i)] = *len >> (i*8);
    *len = new_len + 8;
    return res;
}

char*   blocks_to_hex(uint32_t *blocks, int len, int endian)
{
    char    *res;
    uint8_t *p;
    int     index;
    int     j;
    int     b;

    res = ft_strnew(len * 8);
    ft_memset(res, '0', len * 8);
    index = 0;
    b = -1;
    while (++b < len)
    {
        p = (uint8_t*)&blocks[b];
        (endian == 1) ? (p += 3) : 0;
        j = -1;
        while (++j < 4)
        {
            res[++index] = (*p % 16) + ((*p % 16 > 9) ? 'a' - 10 : '0');
            *p /= 16;
            res[index++ - 1] = (*p % 16) + ((*p % 16 > 9) ? 'a' - 10 : '0');
            p += (endian == 1) ? -1 : 1;
        }
    }
    return res;
}

char*   c_case(char *s, int mod)
{
    int i;

    i = -1;
    while (s[++i] != '\0')
        s[i] = (mod == 1) ? ft_toupper(s[i]) : ft_tolower(s[i]);
    return s;
}

void p(char *s1, char *s2, char *s3, char *s4, char *s5)
{
    write(1, s1, ft_strlen(s1));
    write(1, s2, ft_strlen(s2));
    write(1, s3, ft_strlen(s3));
    write(1, s4, ft_strlen(s4));
    write(1, s5, ft_strlen(s5));
}

void    print_res(char *c, t_op *op, int len)
{
    int     r;
    int     q;
    
    r = 0;
    q = 0;
    while (len-- > 0 && op != NULL)
    {
        (op->f.r == 1) ? (r = 1) : 0;
        (op->f.q == 1) ? (q = 1) : 0;
        if (op->err != NULL)
            p("ft_ssl: ", c_case(c, 2), ": ", op->err, "");
        else if (op->f.p == 1)
            p(op->inp, op->out, "", "", "");
        else if (op->n != NULL && q != 1 && r != 1)
            (op->f.s == 1) ? p(c_case(c, 1), " (\"", op->n, "\") = ", op->out) : p(c_case(c, 1), " (", op->n, ") = ", op->out);
        else if (op->n != NULL && q != 1 && r == 1)
            (op->f.s == 1) ? p(op->out, " \"", op->n, "\"", "") : p(op->out, " ", op->n, "", "");
        else
            write(1, op->out, ft_strlen(op->out));
        write(1, "\n", 1);
        op = op->next;
    }
}