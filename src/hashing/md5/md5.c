#include "md5_header.h"
#include "hidden.h"

static void md5_operate_block(uint32_t *a, uint32_t *b, uint32_t *c, uint32_t *d, uint32_t *msg)
{
    int         i;
    uint32_t    f;

    f = 0;
    i = -1;
    while (++i < 64)
    {
        if (i < 16)
            f = F(*b, *c, *d);
        else if (i < 32)
            f = G(*b, *c, *d);
        else if (i < 48)
            f = H(*b, *c, *d);
        else
            f = I(*b, *c, *d);
        DO(a, b, msg[msg_index[i]], shift[i], md5_k[i], f);
        ROTATE(a, b, c, d);
    }
}

char*   md5(char *msg, size_t len)
{
    uint32_t    tmp[4];
    uint32_t    r[4];
    size_t      i;

    msg = (char*)padding(msg, &len, 1);
    ft_memcpy(r, md5_init, 16);
    i = 0;
    while (i < len)
    {
        ft_memcpy(tmp, r, 16);
        md5_operate_block(&tmp[0], &tmp[1], &tmp[2], &tmp[3], (uint32_t*)(msg + i));
        ADD_TO_RES(r, tmp);
        i += 64;
    }
    free(msg);
    return  blocks_to_hex(r, 4, -1);
}