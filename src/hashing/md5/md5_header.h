#ifndef MD5_HEADER_H
#define MD5_HEADER_H

#include <unistd.h>

#define MD5 "md5"

char*   md5(char *data, size_t len);

#endif