#include "sha256_header.h"
#include "hidden.h"

static void    sha256_block(uint32_t *tmp, uint8_t *t)
{
    uint32_t    data[64];
    uint32_t    t1;
    uint32_t    t2;
    int         i;
    
    i = -1;
    while (++i < 16)
        data[i] = (t[i * 4] << 24) | (t[(i * 4) + 1] << 16) | (t[(i * 4) + 2] << 8) | (t[(i * 4) + 3]);
    i = 15;
    while (++i < 64)
        data[i] = data[i - 16] + S0(data[i - 15]) + data[i - 7] + S1(data[i - 2]);
    i = -1;
    while (++i < 64) 
    {
        t2 = EP0(tmp[0]) + MA(tmp[0], tmp[1], tmp[2]);
        t1 = tmp[7] + EP1(tmp[4]) + CH(tmp[4], tmp[5], tmp[6]) + sha256_k[i] + data[i];
        tmp[7] = tmp[6];
        tmp[6] = tmp[5];
        tmp[5] = tmp[4];
        tmp[4] = tmp[3] + t1;
        tmp[3] = tmp[2];
        tmp[2] = tmp[1];
        tmp[1] = tmp[0];
        tmp[0] = t1 + t2;
    }
}

char*   sha256(char *msg, size_t len)
{
    uint32_t    tmp[8];
    uint32_t    r[8];
    size_t      i;
    int         j;

    msg = (char*)padding(msg, &len, -1);
    ft_memcpy(r, sha256_init, 32);
    i = 0;
    while (i < len)
    {
        ft_memcpy(tmp, r, 32);
        sha256_block(tmp, (uint8_t*)(msg + i));
        j = -1;
        while (++j < 8)
            r[j] += tmp[j];
        i += 64;
    }
    free(msg);
    return  blocks_to_hex(r, 8, 1);
}