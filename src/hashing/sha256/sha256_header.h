#ifndef SHA256_HEADER_H
#define SHA256_HEADER_H

#include <unistd.h>

#define SHA256 "sha256"

char*   sha256(char *data, size_t len);

#endif