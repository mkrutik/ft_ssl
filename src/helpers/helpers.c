#include "helpers_header.h"

void error(char *error)
{
    write(1, "ft_ssl: Error: ", 15);
    write(1, error, ft_strlen(error));
    write(1, "\n\n", 2);
    write(1, "Standard commands:\n\n", 20);
    write(1, "Message Digest commands:\nmd5\nsha256\n\n", 37);
    write(1, "Cipher commands:\nbase64\ndes\ndes-ecb\ndes-cbc\n", 44);
    exit(-1);
}

void usage()
{
    write(1, "usage: ft_ssl command [command opts] [command args]\n", 52);
}

char*   read_in(char *fname, size_t *len)
{
    char    *res;
    char    *p;
    char    tmp[10000];
    int     fd;
    size_t  i;

    *len = 0;
    fd = open(fname, O_RDONLY);
    if (fname != NULL && fd < 3)
        return NULL;
    (fname == NULL) ? (fd = 0) : 0;
    res = "";
    while ((i = read(fd, tmp, 9999)) > 0)
    {
        p = res;
        res = ft_strnew(*len + i);
		ft_memcpy(res, p, *len);
		ft_memcpy(res + *len, tmp, i);
		(*len != 0) ? free(p) : 0;
        *len += i;
    }
    (fname != NULL) ? close(fd) : 0;
    return res;
}

int     write_to_file(char *fname, char *str, size_t len)
{
    int fd;

    fd = open(fname,  O_WRONLY | O_CREAT, 0755);
    if (fd < 3 && fname != NULL)
        return -1;
    (fname == NULL) ? (fd = 1) : 0;
    write(fd, str, ft_strlen(str));
    write(fd, "\n", 1);
    (fd != 1) ? close(fd) : 0;
    return len + 1;
}