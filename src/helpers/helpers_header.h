#ifndef HELPERS_HEADER_H
#define HELPERS_HEADER_H

#include <unistd.h>
#include <fcntl.h>

#include "../libft/libft.h"

char*   read_in(char *fname, size_t *len);
int     write_to_file(char *fname, char *str, size_t len);
void    usage();
void    error(char *error);


#endif