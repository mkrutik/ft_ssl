#include "header.h"

int main(int argc, char **argv)
{
	if (argc >= 2)
	{
		if (!ft_strcmp(argv[1], MD5) || !ft_strcmp(argv[1], SHA256))
			hashing(argv[1], argv, argc);
		else if (!ft_strcmp(argv[1], BASE64) || !ft_strcmp(argv[1], DES) || 
			!ft_strcmp(argv[1], DES_ECB) || !ft_strcmp(argv[1], DES_CBC))
			cipher(argv, argc);
		else
			error(ft_strjoin(argv[1], " is an invalid command."));
	}
	else
		usage();
    return 0;
}